module.exports = {
	root: true,
	parserOptions: {
		sourceType: "module",
		parser: "babel-eslint"
	},
	env: {
		browser: true,
		amd: true,
		node: true
	},
	extends: [
		"prettier",
		"plugin:vue/recommended",
		"eslint:recommended",
		"prettier/vue",
		"plugin:prettier/recommended"
	],
	plugins: ["vue", "prettier"],
	rules: {
		"prettier/prettier": "error",
		"vue/max-attributes-per-line": [
			2,
			{
				singleline: 20,
				multiline: {
					max: 1,
					allowFirstLine: false
				}
			}
		],
		"no-console": "off",
		"no-restricted-syntax": [
			"error",
			{
				selector:
					"CallExpression[callee.object.name='console'][callee.property.name!=/^(log|warn|error|info|trace)$/]",
				message: "Unexpected property on console object was called"
			}
		]
	}
};
