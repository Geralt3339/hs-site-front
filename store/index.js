export const state = () => ({
  navItems: [
    {
      title: "Сокращение временных издержек",
      description:
        "Автоматизация процессов позволяет не только переложить часть функций на машины, но и ускорить процессы",
      childs: [
        {
          title: "Система заявок на закупку",
          path: "/reducingTime/purchaseRequesition",
          key: "r1",
          sliders: [0, 0]
        },
        {
          title: "Система проведения тендерных процедур",
          path: "/reducingTime/tenderProcedure",
          key: "r2",
          sliders: [0, 1]
        },
        {
          title: "Система организации грузоперевозок",
          path: "/reducingTime/cargoTransfer",
          key: "r3",
          sliders: [0, 2]
        },
        {
          title: "Система коммуникаций",
          path: "/reducingTime/communicationSystem",
          key: "r4",
          sliders: [0, 3]
        },
        {
          title: "Система сбора и мониторинга показателей",
          path: "/reducingTime/resultsMonitoring",
          key: "r5",
          sliders: [0, 4]
        },
        {
          title: "Визуализация объектов на карте",
          path: "/reducingTime/mapVisualization",
          key: "r6",
          sliders: [0, 5]
        },
        {
          title: "Разработка дашборда для топ-менеджмента",
          path: "/reducingTime/dashboardDevelopment",
          key: "r7",
          sliders: [0, 6]
        },
        {
          title:
            "Система анализа данных, интегрированная с корпоративной системой сбора информации",
          path: "/reducingTime/dataAnalysis",
          key: "r8",
          sliders: [0, 7]
        },
        {
          title:
            "Система мониторинга взаимоотношения с государственными органами",
          path: "/reducingTime/governmentMonitoring",
          key: "r9",
          sliders: [0, 8]
        },
        {
          title: "Система мониторинга бурения, связанная с телеметрией",
          path: "/reducingTime/drillingMonitoring",
          key: "r10",
          sliders: [0, 9]
        },
        {
          title: "Система планирования бурения",
          path: "/reducingTime/drillingPlanning",
          key: "r11",
          sliders: [0, 10]
        },
        {
          title: "Система документооборота служебных записок",
          path: "/reducingTime/workflowSystem",
          key: "r12",
          sliders: [0, 11]
        },
        {
          title: "Система отслеживания статуса задач",
          path: "/reducingTime/tasksMonitoring",
          key: "r13",
          sliders: [0, 12]
        }
      ]
    },
    {
      title: "Повышение гибоксти бизнес-процессов",
      description:
        "В эпоху быстро изменяющихся вводных условий перед бизнесом стоит задача оперативно адаптироваться и корректировать свои процессы",
      childs: [
        {
          title: "Система заявок на закупку",
          path: "/buisnessFlexability/purchaseRequesition",
          key: "b1",
          sliders: [1, 0]
        },
        {
          title: "Система проведения тендерных процедур",
          path: "/buisnessFlexability/tenderProcedure",
          key: "b2",
          sliders: [1, 1]
        },
        {
          title: "Система коммуникаций",
          path: "/buisnessFlexability/communicationSystem",
          key: "b3",
          sliders: [1, 2]
        },
        {
          title: "Система сбора и мониторинга показателей",
          path: "/buisnessFlexability/resultsMonitoring",
          key: "b4",
          sliders: [1, 3]
        },
        {
          title: "Разработка дашборда для топ-менеджмента",
          path: "/buisnessFlexability/dashboardDevelopment",
          key: "b5",
          sliders: [1, 4]
        },
        {
          title:
            "Система анализа данных, интегрированная с корпоративной системой сбора информации",
          path: "/buisnessFlexability/dataAnalysis",
          key: "b6",
          sliders: [1, 5]
        },
        {
          title: "Система отслеживания статуса задач",
          path: "/buisnessFlexability/tasksMonitoring",
          key: "b7",
          sliders: [1, 6]
        }
      ]
    },
    {
      title: "Усиление контроля",
      description:
        "Внедрение систем, позволяющих усиливать контроль как за рабочим временем, так и за отлаженностью процессов, позволяет значительно повысить эффективность труда",
      childs: [
        {
          title: "Система заявок на закупку",
          path: "/enhancedControl/purchaseRequesition",
          key: "e1",
          sliders: [2, 0]
        },
        {
          title: "Система проведения тендерных процедур",
          path: "/enhancedControl/tenderProcedure",
          key: "e2",
          sliders: [2, 1]
        },
        {
          title: "Система организации грузоперевозок",
          path: "/enhancedControl/cargoTransfer",
          key: "e3",
          sliders: [2, 2]
        },
        {
          title: "Система сбора и мониторинга показателей",
          path: "/enhancedControl/resultsMonitoring",
          key: "e4",
          sliders: [2, 3]
        },
        {
          title: "Разработка дашборда для топ-менеджмента",
          path: "/enhancedControl/dashboardDevelopment",
          key: "e5",
          sliders: [2, 4]
        },
        {
          title:
            "Система анализа данных, интегрированная с корпоративной системой сбора информации",
          path: "/enhancedControl/dataAnalysis",
          key: "e6",
          sliders: [2, 5]
        },
        {
          title:
            "Система мониторинга взаимоотношения с государственными органами",
          path: "/enhancedControl/governmentMonitoring",
          key: "e7",
          sliders: [2, 6]
        },
        {
          title: "Система планирования бурения",
          path: "/enhancedControl/drillingPlanning",
          key: "e8",
          sliders: [2, 7]
        },
        {
          title: "Система документооборота служебных записок",
          path: "/enhancedControl/workflowSystem",
          key: "e9",
          sliders: [2, 8]
        },
        {
          title: "Система отслеживания статуса задач",
          path: "/enhancedControl/tasksMonitoring",
          key: "e10",
          sliders: [2, 9]
        }
      ]
    },
    {
      title: "Снижение рисков",
      description:
        "В ходе функционирования компании неизбежно встает вопрос об управлении рисками. Внедрение систем автоматизации позволяет значительно минимизировать последствия.",
      childs: [
        {
          title: "Система заявок на закупку",
          path: "/riskReduction/purchaseRequesition",
          key: "k1",
          sliders: [3, 0]
        },
        {
          title: "Система проведения тендерных процедур",
          path: "/riskReduction/tenderProcedure",
          key: "k2",
          sliders: [3, 1]
        },
        {
          title: "Система организации грузоперевозок",
          path: "/riskReduction/cargoTransfer",
          key: "k3",
          sliders: [3, 2]
        },
        {
          title: "Система коммуникаций",
          path: "/riskReduction/communicationSystem",
          key: "k4",
          sliders: [3, 3]
        },
        {
          title: "Разработка дашборда для топ-менеджмента",
          path: "/riskReduction/dashboardDevelopment",
          key: "k5",
          sliders: [3, 4]
        },
        {
          title:
            "Система анализа данных, интегрированная с корпоративной системой сбора информации",
          path: "/riskReduction/dataAnalysis",
          key: "k6",
          sliders: [3, 5]
        },
        {
          title:
            "Система мониторинга взаимоотношения с государственными органами",
          path: "/riskReduction/governmentMonitoring",
          key: "k7",
          sliders: [3, 6]
        },
        {
          title: "Система мониторинга бурения, связанная с телеметрией",
          path: "/riskReduction/drillingMonitoring",
          key: "k8",
          sliders: [3, 7]
        },
        {
          title: "Система документооборота служебных записок",
          path: "/riskReduction/workflowSystem",
          key: "k9",
          sliders: [3, 8]
        },
        {
          title: "Система отслеживания статуса задач",
          path: "/riskReduction/tasksMonitoring",
          key: "k10",
          sliders: [3, 9]
        }
      ]
    },
    {
      title: "Повышение комфорта сотрудников",
      description:
        "Удобство работы в системах - один из немаловажных факторов, напрямую влияющих на мотивацию персонала и на качество его работы",
      childs: [
        {
          title: "Система заявок на закупку",
          path: "/comfortIncrease/purchaseRequesition",
          key: "c1",
          sliders: [4, 0]
        },
        {
          title: "Система проведения тендерных процедур",
          path: "/comfortIncrease/tenderProcedure",
          key: "c2",
          sliders: [4, 1]
        },
        {
          title: "Система организации грузоперевозок",
          path: "/comfortIncrease/cargoTransfer",
          key: "c3",
          sliders: [4, 2]
        },
        {
          title: "Система коммуникаций",
          path: "/comfortIncrease/communicationSystem",
          key: "c4",
          sliders: [4, 3]
        },
        {
          title: "Система сбора и мониторинга показателей",
          path: "/comfortIncrease/resultsMonitoring",
          key: "c5",
          sliders: [4, 4]
        },
        {
          title: "Визуализация объектов на карте ",
          path: "/comfortIncrease/mapVisualization",
          key: "c6",
          sliders: [4, 5]
        },
        {
          title: "Разработка дашборда для топ-менеджмента",
          path: "/comfortIncrease/dashboardDevelopment",
          key: "c7",
          sliders: [4, 6]
        },
        {
          title:
            "Система анализа данных, интегрированная с корпоративной системой сбора информации",
          path: "/comfortIncrease/dataAnalysis",
          key: "c8",
          sliders: [4, 7]
        },
        {
          title: "Система планирования бурения",
          path: "/comfortIncrease/drillingPlanning",
          key: "c9",
          sliders: [4, 8]
        },
        {
          title: "Система документооборота служебных записок",
          path: "/comfortIncrease/workflowSystem",
          key: "c10",
          sliders: [4, 9]
        }
      ]
    }
  ]
});

export const getters = {
  getNavItems: state => state.navItems
};
